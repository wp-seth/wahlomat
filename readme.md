# NAME

womat use data of wahlomat.de (bpb) from CLI.

# DESCRIPTION

this program uses json files to compare different political positions. on the 
one hand there are the positions of political parties, on the other hand there is 
your personal position. all positions are compared with each other.

# INSTALLATION

    git clone https://gitlab.com/wp-seth/womat.git
    cd womat
    git clone https://github.com/gockelhahn/qual-o-mat-data.git

# SYNOPSIS

womat \[options\]

    -d, --details              print comparison of each single opinion
    -e, --election=string      short name of the election to use (e.g. btw2017), 
                                default = ltw2018_bay
    -g, --generate             generate a json file with your personal positions
    -l, --list                 list all saved elections
        --list-parties         list all parties of current election
    -m, --my-pos=jsonfile      file that contains your position,
                                default = wahlomat_personal.json
    -p, --parties-pos=json/dir file or directory that contains the position of the parties, 
                                default = qual-o-mat-data or wahlomat.json 

meta:

    -V, --version              display version and exit.
    -h, --help                 display brief help
        --man                  display long help (man page)
    -q, --silent               same as --verbose=0
    -v, --verbose              same as --verbose=1 (default)
    -vv,--very-verbose         same as --verbose=2
    -v, --verbose=x            grade of verbosity
                                x=0: no output
                                x=1: default output
                                x=2: much output

some examples:

    womat -g
     same as 
       womat --my-pos=wahlomat_personal.json --generate
     i.e., create a json file with your personal positions

    womat
     same as 
      womat --my-pos=wahlomat_personal.json --parties-pos=wahlomat.json
     i.e., start comparing positions and print results to stdout

    womat -l
     list available elections

    womat -e btw2017 --my-pos=my_position.json
     compare your position with the position of all parties for election btw2017

# OPTIONS

- **--details**, **-d**

    print more details in results comparing each single opinion.

- **--election**=_string_, **-e** _string_

    short name of election to be chosen, e.g. 'btw2017'. see **--list** for full list of 
    possible values.

- **--generate**=_jsonfile_, **-g** _jsonfile_

    generate a json file with you political positions (regarding the wahlomat 
    questions).

- **--list**, **-l**

    list all elections that are available via json file given by **--parties-pos**.

- **--list-parties**

    list all parties of a election chosen by **--election**.

- **--my-pos**=_jsonfile_, **-m** _jsonfile_

    file _jsonfile_ contains your position regarding the wahlomat questions.
    this file should have been written in the format

    {election\_name : string with one character each question}

    v = yes, x = no, n = neutral, ' ' = skip, uppercase = double weighted", e.g., 

        {
        "ltw2018_bay"   : "vXvxVnvvxvnv xV vXVx Vvvvv Vv xVXv X v"
        }

    default: _jsonfile_ = 'wahlomat\_personal.json' 

- **--parties-pos**=_json\_or\_dir_, **-p** _json\_or\_dir_

    _json\_or\_dir_ is either a json file or a copy of the qual-o-mat-data directory 
    containing all wahlomat questions and the positions regarding the wahlomat 
    questions.

    default: _jsonfile_ = 'qual-o-mat-data' if existen, otherwise 'wahlomat.json'.

- **--version**, **-V**

    prints version and exits.

- **--help**, **-h**, **-?**

    prints a brief help message and exits.

- **--man**

    prints the manual page and exits.

- **--verbose**=_number_, **-v** _number_

    set grade of verbosity to _number_. if _number_==0 then no output
    will be given, except hard errors. the higher _number_ is, the more 
    output will be printed. default: _number_ = 1.

- **--silent, --quiet, -q**

    same as **--verbose=0**.

- **--very-verbose, -vv**

    same as **--verbose=3**. you may use **-vvv** for **--verbose=4** a.s.o.

- **--verbose, -v**

    same as **--verbose=2**.

# LICENCE

BSD-2-Clause

Copyright (c) 2018, seth
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

\* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

\* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

originally written by seth
